require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/tc_Idna.rb', 'test/tc_Punycode.rb', 'test/ts_IDN.rb']
end
